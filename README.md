
# doxa-liml assets 
Website assets (e.g. css, javascript) used by Doxa when generating a liturgical website.

The files are © The Orthodox Christian Mission Center

Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
